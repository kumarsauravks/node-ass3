const express = require('express');

const router = express.Router();

const userController = require('../controllers/userController');

router.post('/create-user',userController.postAddUser)
router.post('/update-user/:userId',userController.postUpdateUser)
router.get('/get-users',userController.getUsers)
router.post('/remove-user/:userId',userController.postRemoveUser)
module.exports = router;
