const express = require('express');

const router = express.Router();

const companyController=require('../controllers/companyController')

router.post('/add-company',companyController.postAddCompany)

router.get('/get-companies',companyController.getCompanies)

router.post('/update-company/:compId',companyController.postUpdateCompany)

router.post('/remove-company/:compId',companyController.postDeleteCompany)
module.exports = router;