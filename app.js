const express=require('express');
const path=require('path');
const bodyParser=require('body-parser');
const mongoose=require('mongoose')

const app= express();
const sequelize=require('./util/databaseMySql');
const User=require('./models/user')
const userRoutes=require('./routes/userRoutes')
const companyRoutes=require('./routes/companyRoutes')
app.use(express.static(path.join(__dirname,'public')));
app.use(bodyParser.urlencoded({extended:false}));

app.use(userRoutes);
sequelize
// .sync({ force: true }) // not used in production
.sync()
.then(result=>{
    app.listen(3000)
})
.catch(err=>{
    console.log(err)
})

// app.use(companyRoutes);

// mongoose.connect(
//     // 'mongodb+srv://kumarsauravks:kumarsauravks@cluster0.cnlhr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',{
//     'mongodb+srv://kumarsauravks:kumarsauravks@cluster0.cnlhr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',{
//     useNewUrlParser:true,
//         useCreateIndex:true,
//         useUnifiedTopology:true,
//         retryWrites:true
//     }
// )
// .then(result=>{
//     console.log("Connected to mongoose");
//     app.listen(3000);
// })
// .catch(err=>console.log(err))
