const mongoose=require('mongoose');

const Schema=mongoose.Schema;

const companyScema=new Schema({
    name:{
        type:String,
        required:true
    },
    url:{
        type:String,
        required:true
    },
    location:{
        type:String,
        required:true
    },
    description:{
        type:String,
        required:true
    }
});

module.exports=mongoose.model('Company',companyScema)