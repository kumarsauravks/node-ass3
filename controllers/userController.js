const User = require('../models/user');

exports.postAddUser = (req, res, next) => {
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const phone = req.body.phone;
    if (!firstName || !email || !phone) {
        return res.status(422).json({ message: "Please enter required fields!!" });
    }
    User.create({
        firstName: firstName,
        lastName: lastName,
        email: email,
        phone: phone
    })
        .then(result => {
            console.log(result);
            res.status(200).json({ user: result });
        })
        .catch(err => {
            console.log(err)
        })
}
exports.postUpdateUser = (req, res, next) => {
    const id = req.params.userId;
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const phone = req.body.phone;

    if (!id) {
        return res.status(422).json({ message: "Please pass a user id in the url" })
    }

    User.findByPk(id)
        .then(user => {
            if(firstName){
                user.firstName = firstName;
            }
            if(lastName){
                user.lastName = lastName;
            }
            if(email){
                user.email = email;
            }
            if(phone){
                user.phone = phone;
            }
            user.save();
            res.status(200).json({ message: "user updated successfully!", user: user });
        })
        .catch(err => console.log(err));
}

exports.getUsers = (req, res, next) => {
    User.findAll()
        .then(users => {
            res.status(200).json({ users: users })
        })
        .catch(err => {
            res.json({ err: err })
        })
}

exports.postRemoveUser = (req, res, next) => {
    const id = req.params.userId;
    if (!id) {
        return res.status(422).json({ message: "Please select a user to delete!!" });
    }
    User.findByPk(id)
        .then(user => {
            user.destroy();
            return res.status(200).json({ message: "User Deleted Successfully" });
        })
        .catch(err => console.log(err))
}
