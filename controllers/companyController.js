const Company=require('../models/companies');

exports.postAddCompany=(req,res,next)=>{
    const name=req.body.name;
    const url=req.body.url;
    const location=req.body.location;
    const description=req.body.description;
    const company=new Company({
        name:name,
        url:url,
        location:location,
        description:description
    });
    company.save()
    .then(result=>{
        res.json({company:result})
        console.log('Company Created')
    })
    .catch(err=>console.log(err))
}

exports.getCompanies=(req,res,next)=>{
    Company.find()
        .then(companies=>{
            res.json({companies:companies})
        })
        .catch(err=>console.log(err));
}

exports.postUpdateCompany=(req,res,next)=>{
    const compId=req.params.compId;
    const updatedName=req.body.name;
    const updatedUrl=req.body.url;
    const updatedLocation=req.body.location;
    const updatedDescription=req.body.description;
    
    Company.findById(compId)
    .then(product=>{
        product.name=updatedName;
        product.url=updatedUrl;
        product.location=updatedLocation;
        product.description=updatedDescription;
        product.save()
        res.json({product:product})
    })
    .catch(err=>console.log(err))
}

exports.postDeleteCompany=(req,res,next)=>{
    const compId=req.params.compId;
    Company.findByIdAndDelete(compId)
    .then(()=>{
        res.json("Company Deleted Successfully")
    })
    .catch(err=>console.log(err));
}