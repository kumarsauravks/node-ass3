const Sequelize = require('sequelize');

const sequelize = new Sequelize('node_ass3', 'root', 'password', {
  dialect: 'mysql',
  host: 'localhost'
});

module.exports = sequelize;
